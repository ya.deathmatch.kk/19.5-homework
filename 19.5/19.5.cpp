﻿// 19.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;



class Animal
{

public:
   virtual void Voice()
    {

      
           cout << "Animal\t";

     

        }
   
};


class Dog : public Animal
{

public:

    void Voice() override
    {
        cout << "Dog: Woof! Woof!" << endl;

    }

};


class Cat:public Animal

{
public:
    
   void  Voice() override
    {
        cout << "Cat: Meow!Meow!"<<endl;

    }


};

class Bird :public Animal
{
public:
    void Voice()
        override
    {

        cout << "Bird: Carrr!Carrr!"<<endl;
    }
};

int main()
{
    Animal* p = new Dog;
    p->Voice();
    Animal* d = new Cat;
    d->Voice();
    Animal* b = new Bird;
    b->Voice();

    


    cout << "---------------------------------------------"<<endl;

    Animal* array[3];
    array[0] = new Dog;
    array[1] = new Cat;
    array[2] = new Bird;
    for (int i = 0; i < 3; i++)
    {
        array[i]->Voice();
        
    }
    




}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
